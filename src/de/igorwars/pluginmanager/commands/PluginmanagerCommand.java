package de.igorwars.pluginmanager.commands;

import de.igorwars.pluginmanager.PluginManager;
import org.apache.commons.lang.ObjectUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.io.File;

public class PluginmanagerCommand implements CommandExecutor {

    private PluginManager pluginManager;

    public PluginmanagerCommand(PluginManager manager) {
        pluginManager = manager;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] arguments) {

        if(arguments.length==0) {
            help(commandSender);
            return true;
            //returns Help-Message to the user
        }
        if(arguments.length==1){
            if(arguments[0].equalsIgnoreCase("reload")||arguments[0].equalsIgnoreCase("disable")||arguments[0].equalsIgnoreCase("enable")) {
                //Command -> /pluginmanager reload/disable/enable (Nächstes Argument)
                commandSender.sendMessage("§c/pluginmanager " + arguments[0] + " (PluginName)");
                return true;
            }else help(commandSender);
        }else if(arguments.length==2){
            Plugin plugin;
            switch (arguments[0]){
                case "reload":
                    try {
                        plugin = pluginManager.getServer().getPluginManager().getPlugin(arguments[1]);
                        if(plugin==null) {
                            commandSender.sendMessage("§cThat Plugin does not exist. uffff");
                            break;
                        }
                    }catch (NullPointerException e){


                        commandSender.sendMessage("§cThat Plugin does not exist.");
                        break;
                    }
                    plugin = pluginManager.getServer().getPluginManager().getPlugin(arguments[1]);
                    plugin.reloadConfig();
                    break;
                case "disable":
                    try {
                        plugin = pluginManager.getServer().getPluginManager().getPlugin(arguments[1]);
                        if(plugin==null) {
                            commandSender.sendMessage("§cThat Plugin does not exist. uffff");
                            break;
                        }
                    }catch (NullPointerException e){


                        commandSender.sendMessage("§cThat Plugin does not exist.");
                        break;
                    }
                    plugin = pluginManager.getServer().getPluginManager().getPlugin(arguments[1]);
                    if(plugin.isEnabled()){
                        pluginManager.getServer().getPluginManager().disablePlugin(plugin);
                    }else commandSender.sendMessage("That plugin is already disabled");
                    break;
                case "enable":
                    try {
                        plugin = pluginManager.getServer().getPluginManager().getPlugin(arguments[1]);
                        if(plugin==null) {
                            commandSender.sendMessage("§cThat Plugin does not exist. uffff");
                            break;
                        }
                    }catch (NullPointerException e){


                        commandSender.sendMessage("§cThat Plugin does not exist.");
                        break;
                    }
                    plugin = pluginManager.getServer().getPluginManager().getPlugin(arguments[1]);
                    if(!plugin.isEnabled()){
                        pluginManager.getServer().getPluginManager().enablePlugin(plugin);
                    }else{
                        commandSender.sendMessage("Plugin is already enabled");
                    }
                    break;

                    default:
                        plugin = pluginManager.getServer().getPluginManager().getPlugin(arguments[1]);


            }

        }
        return false;
    }

    public void help(CommandSender sender){

    }
}
