package de.igorwars.pluginmanager;

import de.igorwars.pluginmanager.commands.PluginmanagerCommand;
import org.bukkit.plugin.java.JavaPlugin;

public class PluginManager extends JavaPlugin {

    @Override
    public void onEnable(){
        getCommand("pluginmanager").setExecutor(new PluginmanagerCommand(this));
        System.out.println("PluginManager by Igorwars loaded");
    }

}
